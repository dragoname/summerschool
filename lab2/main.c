#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLENGTH  255

int inp_str(char* strings, int maxlen)
{
	int i=0;
	char c;
	scanf("%c",&c);
	while (i < maxlen-1 && !(c == '\n' || c == '\0'))
	{
		strings[i]=c;
		i++;
		scanf("%c",&c);
	}
	strings[i]='\n'; 
	return i; // символ конца строки не учитывается в длине строки
}

int inp_str1(char* strings, int maxlen)
{
	scanf("%s", strings);
	int i = strlen(strings);
	strings[i]='\n';
	
	return i;
}

void out_str(char* string, int length, int number)
{
	int i=0;
	while (i < length && !(string[i] == '\n' || string[i] == '\0'))
	{
		printf ("%c",string[i]);
		i++;
	}
	printf("\n%d\n",number);
}

void out_str1(char* string, int length, int number)
{
	printf("%s",string);
}

char** init_masS(char **mas, int n)
{
	int i=0,j;
	i++;
	mas=(char**)malloc(n*sizeof(char*));
	if ( !mas)
	{
		printf("error");
		return NULL; 
	}
	for (i=0; i<n; i++)
	{
		mas[i]=malloc(MAXLENGTH*sizeof(char));
		for (j=0; j<MAXLENGTH; j++)
		{
			mas[i][j]='\0';
		}
	}
	return mas;
}

char** input_data(char **mas, int* kol, int *min_length)
{
	int n,i,len, min_len=*min_length;
	char trash;
	scanf("%d%c",&n, &trash);
	*kol=n;	
	mas=init_masS(mas,n);	
	for (i=0; i<n; i++)
	{
		scanf("%s",mas[i]);
		len=strlen(mas[i]);
		mas[i][len]='\n';
		if(len < min_len)
			min_len = len;
		//printf("%s",mas[i]);
	}
	*min_length = min_len;
	return mas;
}

void out_task(int kol_rev, int min_len, char **mas, int n)
{
	/*int i;
	for(i=0; i<n; i++)
	{
		out_str1(mas[i], MAXLENGTH, i);	
	}*/
	printf("%d\n%d\n",kol_rev,min_len);
}

void clean (char** s, int n)
{
	/*int i;
	for (i=0; i<n; i++)
	{
		free(s[i]);
	}*/
	free(s);
}

int compare(char *x1, char*x2)
{
	char *s1 = (char*) x1;
	char *s2 = (char*) x2;
	int l1,l2;
	l1 = strlen( s1 );
	printf("%s",s1);
	l2 = strlen( s2 );
	printf("%s",s2);
	printf("%d %d\n",l1,l2);
	return l1 - l2;
	//return ( strlen( *(char*)x1 ) - strlen( *(char*)x2 ) );
	
}

int compare1(const void *x1, const void *x2)
{
	char *i = (char*) x1;
	char *k = (char*) x2;
	printf("%c %c\n",i[0],k[0]);
	return (i[0]-k[0]);
}

int mycompare(char *x1, char*x2)
{
	int l1,l2;
	l1 = strlen( x1 );
	l2 = strlen( x2 );
	return l1 - l2;
	
}

int sorted(char **mas, int n)
{
	int i,j,rev=0;
	char *temp;
	for (i=0; i<n-1; i++)
	{
		for (j=i+1; j<n; j++)
		{
			if ( mycompare( mas[i], mas[j]) > 0 )
			{
				temp=mas[i];
				mas[i]=mas[j];
				mas[j]=temp;
				rev++;
			}
		}
	}
	return rev;
}

int main()
{
	char **masS=NULL;
	int  kol=1, min_length=MAXLENGTH+1, kol_reverse=0;
	//ввод данных
	masS = input_data(masS, &kol, &min_length); //
	//сортировка
	//printf("\nсортировка\n\n");
	kol_reverse=sorted(masS,kol);
	//вывод данных
	out_task(kol_reverse, min_length, masS, kol);
	clean(masS,kol);
	return 0;
}
